/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polyabstract;

import javax.swing.*;

/**
 *
 * @author Richard
 */
public class Nacional extends Paquete{
    
    private double precio;

    public Nacional(String nombre, String destino, String pago, String telefono, int personas, String email,double precio) {
        super(nombre, destino, pago, telefono, personas, email);
        this.precio = precio;
    }
    
    @Override
    protected String imprimirPaquete(){
        String mensaje="";
        mensaje+="Nombre del comprador: "+this.getNombre()+"\n";
        mensaje+="Destino del paquete: "+this.getDestino()+"\n";
        mensaje+="Forma de pago: "+this.getPago()+"\n";
        mensaje+="Nombre del comprador: "+this.getNombre()+"\n";
        mensaje+="Teléfono del comprador: "+this.getTelefono()+"\n";
        mensaje+="Cantidad de personas en el viaje: "+this.getPersonas()+"\n";
        mensaje+="Email del comprador: "+this.getEmail()+"\n";
        mensaje+="Precio del paquete: "+this.precio+"\n\n";
        return mensaje;
    };
       
    
}
