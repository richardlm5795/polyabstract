/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polyabstract;

import javax.swing.JOptionPane;

/**
 *
 * @author Richard
 */
public class Internacional extends Paquete{
    
    private String nombreHotel;
    private double precioHotel;
    private String aerolinea;
    private double precioTicket;
    private int dias;

    public Internacional(String nombre, String destino, String pago, String telefono, int personas, String email, String nombreHotel, double precioHotel, String aerolinea, double precioTicket, int dias) {
        super(nombre, destino, pago, telefono, personas, email);
        this.nombreHotel = nombreHotel;
        this.precioHotel = precioHotel;
        this.aerolinea = aerolinea;
        this.precioTicket = precioTicket;
        this.dias = dias;
    }
    
    @Override
    protected String imprimirPaquete(){
        String mensaje="";
        mensaje+="Nombre del comprador: "+this.getNombre()+"\n";
        mensaje+="Destino del paquete: "+this.getDestino()+"\n";
        mensaje+="Forma de pago: "+this.getPago()+"\n";
        mensaje+="Nombre del comprador: "+this.getNombre()+"\n";
        mensaje+="Teléfono del comprador: "+this.getTelefono()+"\n";
        mensaje+="Cantidad de personas en el viaje: "+this.getPersonas()+"\n";
        mensaje+="Email del comprador: "+this.getEmail()+"\n";
        mensaje+="Nombre del hotel: "+this.nombreHotel+"\n";
        mensaje+="Precio del hotel: "+this.precioHotel+"\n";
        mensaje+="Nombre de la aerolinea: "+this.aerolinea+"\n";
        mensaje+="Precio del ticket: "+this.precioTicket+"\n";
        mensaje+="Dias del viaje: "+this.dias+"\n";
        return mensaje;
    };
    
}
