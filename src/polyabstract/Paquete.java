package polyabstract;

public abstract class Paquete {    
    private String nombre;
    private String destino;
    private String pago;
    private String telefono;
    private int personas;
    private String email;
    
    public Paquete(String nombre, String destino, String pago, String telefono, int personas, String email) {
        this.nombre = nombre;
        this.destino = destino;
        this.pago = pago;
        this.telefono = telefono;
        this.personas = personas;
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getPersonas() {
        return personas;
    }

    public void setPersonas(int personas) {
        this.personas = personas;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    protected abstract String imprimirPaquete();    
    
}
