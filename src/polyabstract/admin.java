package polyabstract;

import javax.swing.*;

public class admin {
    
    private Paquete[] paquetes;
    private int paquetesComprados;
    
    public admin( ) {
        paquetes = new Paquete[30];
    };
    
    public void menu(){
        String mensaje = "¡Bienvenido a la Agencia de Viajes El Nombre Que Ud Quiera!\n";
        mensaje+="Si desea comprar un paquete, introduzca 1.\n";
        mensaje+="Si desea imprimir su factura de compra y luego salir del sistema, presione 0";
        int opcion = 0;
        do{
            opcion = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
            switch (opcion){
                case 1:
                    paquetes[paquetesComprados++]=crearPaquete();
                    JOptionPane.showMessageDialog(null, "El paquete ha sido agregado");
                    break;
                case 0:
                    for (int i=0;i<paquetesComprados;i++){
                        JOptionPane.showMessageDialog(null,"El paquete "+(i+1)+" consta de:\n\n"+ paquetes[i].imprimirPaquete());
                    }
                    JOptionPane.showMessageDialog(null,"¡Gracias por su compra!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null,"Opción inválida");                    
            }
        }while (opcion!=0);
                
    }
    
    public Paquete crearPaquete(){
        String mensaje = "Si desea comprar un paquete nacional, introduzca 1.\n";
        mensaje+="Si desea comprar un paquete internacional, presione 2";
        int opcion = 0;
        opcion = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
        while ((opcion!=1) && (opcion!=2)){
            JOptionPane.showMessageDialog(null,"Opción inválida");
            opcion = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
        }
        String nombre = JOptionPane.showInputDialog("Introduzca su nombre completo:");
        String destino = JOptionPane.showInputDialog("Introduzca el destino del paquete:");
        String pago = JOptionPane.showInputDialog("Introduzca el método de pago:");
        String telefono = JOptionPane.showInputDialog("Introduzca su teléfono:");
        int personas = Integer.parseInt(JOptionPane.showInputDialog("Introduzca la cantidad de personas que van a viajar con ud:"));
        String email = JOptionPane.showInputDialog("Introduzca su email:");
        if (opcion==1){
            double precio = Double.parseDouble(JOptionPane.showInputDialog("Introduzca el precio de su paquete:"));
            return new Nacional(nombre,destino,pago,telefono,personas,email,precio);
        }
        else{
            
            String nombreHotel = JOptionPane.showInputDialog("Introduzca el nombre del hotel:");
            double precioHotel = Double.parseDouble(JOptionPane.showInputDialog("Introduzca el precio del hotel:"));
            String aerolinea = JOptionPane.showInputDialog("Introduzca el nombre de la aerolinea:");
            double precioTicket = Double.parseDouble(JOptionPane.showInputDialog("Introduzca el precio del ticket: "));
            int dias = Integer.parseInt(JOptionPane.showInputDialog("Introduzca la cantidad de días del viaje: "));
            return new Internacional(nombre,destino,pago,telefono,personas,email,nombreHotel,precioHotel,aerolinea,precioTicket,dias);
        }
    }
    
}    


    